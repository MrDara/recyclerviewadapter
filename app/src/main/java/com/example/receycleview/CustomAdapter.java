package com.example.receycleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {


    Mymovie [] mymovies;
    Context context;

    List<Mymovie> mymovieList;

    public CustomAdapter(List<Mymovie> mymovieList,MainActivity context) {
//        this.mymovies = mymovies;

        this.mymovieList = mymovieList;
        this.context = context;



    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater =LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_receycle_view_list,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        final Mymovie postmovie = mymovies[position];
        final Mymovie postmovie = mymovieList.get(position);
        holder.imageView.setImageResource(postmovie.getImageView());
        holder.Caption.setText(postmovie.getCaption());
        holder.Description.setText(postmovie.getDescription());

        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Caption : "+postmovie.getCaption(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mymovieList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView Caption, Description,close;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            close = itemView.findViewById(R.id.close_icon);
            imageView = itemView.findViewById(R.id.image_view);
            Caption = itemView.findViewById(R.id.txt_caption);
            Description = itemView.findViewById(R.id.txt_description);

        }
    }
}
