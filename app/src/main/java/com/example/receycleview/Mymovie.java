package com.example.receycleview;

public class Mymovie {

    private String Caption;
    private String Description;
    private int imageView;

    public Mymovie(String caption, String description, int imageView) {
        Caption = caption;
        Description = description;
        this.imageView = imageView;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String caption) {
        Caption = caption;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getImageView() {
        return imageView;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }
}
