package com.example.receycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    RecyclerView recyclerView;
//    Mymovie[] mymovies;


    List<Mymovie> mymovieList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.RecyclerView_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        assignValue();


    }

//    public void assignValue(){
//        Mymovie mymovie[] ={
//                new Mymovie("Avengers","This is movie",R.drawable.avenger1),
//                new Mymovie("Avengers 1","This is movie",R.drawable.avenger2),
//                new Mymovie("Avengers 2","This is movie",R.drawable.avenger3),
//                new Mymovie("Avengers 3","This is movie",R.drawable.avenger4)
//        };
//        CustomAdapter customAdapter = new CustomAdapter(mymovie,MainActivity.this);
//        recyclerView.setAdapter(customAdapter);
//    }

    public void assignValue(){
        Mymovie mymovie1 = new Mymovie("Avengers 1",getResources().getString(R.string.desc1),R.drawable.avenger1);
        Mymovie mymovie2 = new Mymovie("Avengers 2",getResources().getString(R.string.desc1),R.drawable.avenger2);
        Mymovie mymovie3 = new Mymovie("Avengers 3",getResources().getString(R.string.desc1),R.drawable.avenger3);
        Mymovie mymovie4 = new Mymovie("Avengers 4",getResources().getString(R.string.desc1),R.drawable.avenger4);
        Mymovie mymovie5 = new Mymovie("Avengers 5",getResources().getString(R.string.desc1),R.drawable.avenger1);
        Mymovie mymovie6 = new Mymovie("Avengers 6",getResources().getString(R.string.desc1),R.drawable.avenger2);
        Mymovie mymovie7 = new Mymovie("Avengers 7",getResources().getString(R.string.desc1),R.drawable.avenger3);
        Mymovie mymovie8 = new Mymovie("Avengers 8",getResources().getString(R.string.desc1),R.drawable.avenger4);
        mymovieList.add(mymovie1);
        mymovieList.add(mymovie2);
        mymovieList.add(mymovie3);
        mymovieList.add(mymovie4);
        mymovieList.add(mymovie5);
        mymovieList.add(mymovie6);
        mymovieList.add(mymovie7);
        mymovieList.add(mymovie8);
        CustomAdapter customAdapter = new CustomAdapter(mymovieList,MainActivity.this);
        recyclerView.setAdapter(customAdapter);
    }
}